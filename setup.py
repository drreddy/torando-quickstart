from setuptools import setup

setup(name='Rajeev Reddy',
      version='0.1',
      description='full stack app',
      author='Rajeev Reddy',
      author_email='rajeev.reddy.d@gmail.com',
      url='http://rajeev-reddy.com/',
      install_requires=['tornado', 'requests', 'beautifulsoup4'],
     )